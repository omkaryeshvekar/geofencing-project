//
//  ViewController.swift
//  Geofencing_Technology
//
//  Created by Omkar on 23/10/18.
//  Copyright © 2018 Omkar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications

class geoFenceViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet var mpview: MKMapView!
    var index = 1
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {(granted, error) in }
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        mpview.removeOverlays(mpview.overlays)
        
        
    }
    
    // MARK: - CLLocationManagerDelegate
    
    //Updated location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationManager.stopUpdatingLocation()
        
        // set current location on the map
        self.mpview.showsUserLocation = true
        
        self.hitLocationsUrlForFedexLocations()
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        let title = "We have your Package ready."
        let message = "Visit the store to pick up."
        self.showAlert(title: title, message: message)
        self.showNotification(title: title, message: message)
    }
    
    
    func hitLocationsUrlForFedexLocations() {
        
        let urlString = "https://places.demo.api.here.com/places/v1/discover/search?at=18.6505%2C73.8086&q=FedEx&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg"
        
        let url = NSURL(string: urlString)
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "GET"
        request.timeoutInterval = 20
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) ->  Void in
            
            if error != nil {
                print(error!)
            } else {
                do {
                    
                    if let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print(parsedData)
                        
                        if let result = parsedData["results"] as? NSDictionary {
                            
                            let locationDetails = result["items"] as! NSArray
                            
                            for index in 0...locationDetails.count - 1 {
                                let myLocationDetail = locationDetails[index] as! NSDictionary
                                let position = myLocationDetail["position"] as! NSArray
                                
                                
                                let latitude = position[0] as! NSNumber
                                let longitude = position[1] as! NSNumber
                                
                                let myAnnotation = MKPointAnnotation()
                                myAnnotation.coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(truncating: latitude),CLLocationDegrees(truncating: longitude))
                                myAnnotation.title = "FedEx"
                                self.mpview.addAnnotation(myAnnotation)
                                
                                // adding region as a geofence.
                                let regionCoordinates = myAnnotation.coordinate
                                self.addGeofenceRegion(regionCoordinates: regionCoordinates)
                                
                            }
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
        })
        dataTask.resume()
    }
    
    
    func addGeofenceRegion(regionCoordinates: CLLocationCoordinate2D) {
        
        let coordId : Double = regionCoordinates.latitude
        let stringId = String(format: "%.5f", coordId).components(separatedBy: ".").last ?? "Unexpected" ; print(stringId)
        
        DispatchQueue.main.async {
            let region = CLCircularRegion(center: regionCoordinates, radius: 500, identifier: stringId)
            self.locationManager.startMonitoring(for: region)
            let circle = MKCircle(center: regionCoordinates, radius: region.radius)
            self.mpview.add(circle)
        }
    }
    
    
    func showAlert(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    
    func showNotification(title : String, message : String) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.badge = 1
        content.sound = .default()
        let request = UNNotificationRequest(identifier: "notify", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    
}

extension geoFenceViewController : MKMapViewDelegate {
    
    func mapView(_ mpview: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let circleOverlay = overlay as? MKCircle else { return MKOverlayRenderer() }
        let circleRenderer = MKCircleRenderer(circle: circleOverlay)
        circleRenderer.fillColor = .red
        circleRenderer.alpha = 0.2
        return circleRenderer
    }
    
    func mapView(_ mpView: MKMapView, viewFor myAnnotation: MKAnnotation) -> MKAnnotationView? {
        
        
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(myAnnotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView : MKAnnotationView?
        
        if let dequeAnnotationView = mpview.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeAnnotationView
            annotationView?.annotation = myAnnotation
        } else {
            annotationView = MKAnnotationView(annotation: myAnnotation, reuseIdentifier: annotationIdentifier)
            annotationView?.canShowCallout = true
        }
        
        
        if let annotationView = annotationView {
            annotationView.image = UIImage(named: String(format: "marker_48"))
        }
        
        self.index += 1
        
        
        return annotationView!
        
        
    }
    
}


